package ch.prodigio.vmt.demo.application.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
        @PropertySource("classpath:notification-alert.properties")
})
public class PropertiesSourceConfig {
}