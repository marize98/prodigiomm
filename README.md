# VEHICLE MILES TRAVELED MICROSERVICE
Desarrollo de un microservicio que expone un CRUD conectando a una BD en la que previamente se cargan los datos del dataset público "04_sample_vmt_county.csv" disponible en: https://data.world/associatedpress/vehicle-miles-traveled. 
La API fue documentada con `Open API` y testeada con `Postman`. Se implementarions pruebas unitarias y de integración con `JUnit` y `Mockito`.
Se empleó una arquitectura hexagonal siguiendo `Domain Driver Design` y se aplicaron patrones como Adapter, Repositorios entre otros. 
Para optimizar las consultas se emplea `HATEOAS` con `Paginación`, compresión `gzip` y se ha habilitado `HTTP2`.

### Instalación y despliegue

1. `Intellij IDEA` como IDE de desarrollo. Descargar [aquí](https://www.jetbrains.com/es-es/idea/download/).
2. PostgreSQL. Configurar base de datos, puerto y host en `aplication.properties`
3. `Postman` para probar los endpoint de las APIs. Descargar [aquí](https://www.postman.com/downloads/).
    El directorio `postman` contiene el archivo `Vehicle Miles Traveled.postman_collection.json` con todas las request de los endpoints implementados.
4. `MAVEN` para gestionar las dependencias. Descargar [aquí](https://maven.apache.org/download.cgi/).
   El `pom/xml` contiene todas las dependencias que deben ser descargadas para el despliegue de este proyecto.

### Archivos y clases puntuales
- archivo `application.properties` contiene las configuraciones del proyecto como DB, encoding, ruta para open-api, etc.
- las clases `Documentation` almacenan la documentación, es para simplificar las clases controladoras, dejarlas con el mínimo de doc swagger.
- la clase `URIConstant` es donde he definido las URI del proyecto, para si en un futuro se desean cambiar, ir solo a este archivo en lugar de cada API.
- la clase `VehicleMilesTraveledApiController` es el controlador con la implementación de los endpints.
- la clase `VehicleMilesTraveledOpenApi` es el contrato de la api que contiene las anotaciones para especificar/documentar los endpoints empleando Open Api.
- los servicios se encuentran dividos por casos de uso (domain/use_ase), donde cada caso de uso responde a cada uno de los requerimientos/HU planteados en el CRUD presentado.

### Pruebas 
Se implementaron pruebas automatizadas de unidad y de integración empleando `Junit` y `Mockito`
